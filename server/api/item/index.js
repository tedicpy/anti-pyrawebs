'use strict';

var express = require('express');
var controller = require('./item.controller');
var config = require('../../config/environment');
require('./loop');

var router = express.Router();

if(config.env === 'development') {
  router.get('/rebuild', controller.rebuild);
}

router.get('/:key/get-file', controller.getFile);
router.get('/:key', controller.showByKey);
router.post('/search', controller.search);
// router.put('/:id', controller.upsert);
// router.patch('/:id', controller.patch);
// router.delete('/:id', controller.destroy);

module.exports = router;
