'use strict';

import _ from 'lodash';
import { Feed } from 'feed';
import Item from '../item/item.model';
import Sync from '../item/sync.model';
import { zoteroConfig } from '../../components/zotero';
import moment from 'moment';


function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.error(err);
    res.status(statusCode).send(err);
  };
}

/**
 * single group & year
 * multi tags
 * 
 * ejemplo
 *  http://domain/feeds / rss2/items / ?group=475109&tags=datos-personales__privacidad&year=2019
 *    req.params { feedType: 'rss', resource: 'items' }
 *    req.query  { group: '475109', tags: 'datos-personales__privacidad', year: '2019' }
 */
export function itemsFeed(req, res) {
  // parse query
  // buscar items
    // crear encabezado del feed
      // agregar categorías al feed
    // iterar resultados
      // agregar items al feed

  // vars
  let { feedType, resource } = req.params;
  let { group, tags, year } = req.query;
  const query = {};
  const domain = zoteroConfig.main.domain || req.get('host');
  
  // errors
  if(!_.includes(['rss2', 'atom1'], feedType)) { // ['json1']
    throw new Error(`Feed type not recognized.\nValid options: 'rss2', 'atom1', 'json1'`);
  }
  if(!_.includes(['items'], resource)) {
    throw new Error(`Feed resource not recognized.\nValid options: 'items'`);
  }

  // query filter
  if(!group) {
    group = 'all';
  }
  if(group && group !== 'all') {
    query.group = group;
  } else if (group === 'all') {
    query.group = {$in: _.map(zoteroConfig.groups, 'id')};
  }
  if(tags) {
    query.tags = {$all: tags.split('__')};
  }
  if(year) {
    query.accessDate = {
      $gte: moment().year(year).startOf('year'),
      $lte: moment().year(year).endOf('year')
    };
  }
  // exclude attachments
  query._type = {$ne: 'attachment'};

  // get items
  return Item.find(query)
    .limit(zoteroConfig.feedsLimit)
    .sort({accessDate: -1})
    .exec()
    .then(items => {
      if(items.length) {
        return Sync.getLastSync().then(lastSync => {
          // instance feed
          const feed = new Feed({
            title: zoteroConfig.main.title,
            description: zoteroConfig.main.description,
            id: domain,
            link: domain,
            language: 'es',
            image: `${domain}/assets/images/logo-circulo.svg`,
            copyright: 'Creative Commons BY-SA',
            updated: new Date(lastSync),
            feedLinks: {
              // json: `${domain}/feeds/json1/items`,
              atom: `${domain}/feeds/atom1/items`
            },
            author: {
              name: zoteroConfig.main.author.name,
              email: zoteroConfig.main.author.email,
              link: zoteroConfig.main.author.link
            }
          });
          
          // assign feeds categories
          if(group === 'all') {
            zoteroConfig.groups.forEach(groupDoc => {
              feed.addCategory(groupDoc.name);
            });
          } else if (group) {
            let groupDoc = _.find(zoteroConfig.groups, {id: parseInt(group)});
            if(groupDoc) {
              feed.addCategory(groupDoc.name);
            }
          }
          if(tags) {
            // asignar el nombre de cada categoría
            tags.split('__').forEach(tagSlug => {
              let tagDoc = _.find(zoteroConfig.tags, {id: tagSlug});
              if(tagDoc) {
                feed.addCategory(tagDoc.name);
              }
            });
          }
          if(year) {
            feed.addCategory(year);
          }
          
          // add items
          items.forEach(item => {
            // limitar la cantidad de texto a mostrar
            let description = _.truncate(item.data.abstractNote, {
              'length': zoteroConfig.abstractNoteLength,
              'separator': '[...]'
            });
            feed.addItem({
              id: item.key,
              link: `${domain}/item/${item.key}`,
              title: item.data.title,
              description: description,
              content: description,
              author: [
                {
                  name: item.source,
                  // email: item.x,
                  link: item.data.url
                }
              ],
              date: new Date(item.accessDate),
              published: new Date(item.accessDate)
              // image: item.x
            });
          });
          return feed[feedType]();
        });
      }
    })
    .then(result => {
      if(result && feedType !== 'json1') {
        return res.status(200).send(result);
      }
      // else if(result && feedType === 'json1') {
      //   return res.status(200).json(JSON.parse(result));
      // }
      return res.sendStatus(204);
    })
    .catch(handleError(res));
};