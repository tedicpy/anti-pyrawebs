'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import utilsPagination from 'angular-utils-pagination';

import {
  routeConfig,
  headingRun
} from './app.config';

import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import list from './list/list.component';
import dashboard from './dashboard/dashboard.component';
import header from '../components/header/header'
import sidebar from '../components/sidebar/sidebar';
import constants from './app.constants';
import util from '../components/util/util.module';
import zotero from '../components/zotero/zotero';

import './app.scss';

angular.module('antiPyrawebsApp', [ngCookies, ngResource, ngSanitize, uiRouter, uiBootstrap, navbar,
  footer, header, sidebar, dashboard, main, list, constants, util, zotero, utilsPagination
])
  .config(routeConfig)
  .run(headingRun);

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['antiPyrawebsApp'], {
      strictDi: true
    });
  });
