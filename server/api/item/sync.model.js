'use strict';

import mongoose from 'mongoose';
import _ from 'lodash';

var SyncSchema = new mongoose.Schema({
  key: String,
  version: Number,
  lastSync: Date
});

SyncSchema.statics.setCurrentVersion = function(key, version) {
  if(!_.isString(key)) {
    key = key.toString();
  }
  const lastSync = new Date();
  return this.findOne({key}).then(doc => {
    if(doc) {
      doc.version = version;
      doc.lastSync = lastSync;
      return doc.save();
    } else {
      return this.create({
        key,
        version,
        lastSync
      });
    }
  });
};

SyncSchema.statics.getCurrentVersion = function(key) {
  if(!_.isString(key)) {
    key = key.toString();
  }
  return this.findOne({key}).then(result => {
    if(result) {
      return result.version;
    } else {
      return 0;
    }
  });
};

SyncSchema.statics.getLastSync = function() {
  return this.findOne().then(result => {
    if(result) {
      return result.lastSync;
    }
  });
};

export default mongoose.model('Sync', SyncSchema);
