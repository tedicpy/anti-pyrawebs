import angular from 'angular';

export class FooterComponent {
  /*@ngInject*/
  constructor(zotero) {
    this.zotero = zotero;
  }
}

export default angular.module('directives.footer', [])
  .component('footer', {
    template: require('./footer.html'),
    controller: ['zotero', FooterComponent],
    controllerAs: 'vm'
  })
  .name;
