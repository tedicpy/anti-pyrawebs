'use strict';

import request from 'request-promise';
import appConfig from '../config/environment/shared';
import _ from 'lodash';

export var zoteroConfig = appConfig;

/**
 * params
 *  query = {
 *    selectedGroup: String,
 *    selectedYear: String || Number,
 *    selectedTags: [String],
 *    page: Number (default: 1),
 *    pageSize: Number (default: appConfig.pageSize)
 *  }
 */
export function getItemsByGroup(query) {
  query.page = query.page || 0;
  query.pageSize = query.pageSize || appConfig.pageSize;
  let groupId = _.get(query.selectedGroup, 'id', query.selectedGroup);
  if(!groupId) {
    throw new Error('Debe especificar un grupo');
  }
  let start = query.page * query.pageSize;
  let path = `${appConfig.zoteroDomain}/groups/${groupId}/items?limit=${query.pageSize}&start=${start}&sort=accessDate`;
  if(_.size(query.selectedTags)) {
    let tagIdList = _.map(query.selectedTags, 'id');
    path = `${path}&tag=${tagIdList.join(' || ')}`;
  }
  if(query.selectedYear) {
    path = `${path}&qmode=titleCreatorYear&q=${query.selectedYear}`;
  }
  return request({
    method: 'GET',
    uri: path,
    json: true
  });
}

export function iterateAllPagesByGroup(groupId, cb, limitPage = null, itemsPerPage, page = 0) {
  let items;
  return getItemsByGroup({
    page,
    pageSize: itemsPerPage || appConfig.pageSize,
    selectedGroup: groupId
  })
  .then(result => {
    items = result;
    return cb(items);
  })
  .then(() => {
    if(limitPage !== null && page === limitPage) {
      return;
    }
    if(items.length === itemsPerPage) {
      return iterateAllPagesByGroup(groupId, cb, limitPage, itemsPerPage, ++page);
    }
  });
}

// console.log('Zotero API test');
// getItemsByGroup({
//   selectedGroup: zoteroConfig.groups[0].id
// }).then(result => {
//   console.log('getItemsByGroup', result.length);
// });
