'use strict';

import Promise from 'bluebird';
import Item from './item.model';
import Pivot from '../viz/pivot.model';
import Sync from './sync.model';
import request from 'request-promise';
import _ from 'lodash';
import moment from 'moment';
import { zoteroConfig } from '../../components/zotero';
import config from '../../config/environment';

let interval = zoteroConfig.syncInterval;
if(config.env === 'development') {
  interval = {
    seconds: 10
  };
}

setTimeout(() => {
  Item.seedIfEmpty()
    .then(added => {
      if(added) {
        return Pivot.rebuild();
      }
    })
    .then(() => {
      sync();
    });
}, 5000);

function sync() {
  // console.log('UPDATING');
  syncGroups().then(result => {
    console.info('SYNC RESULT\n', result);

    setTimeout(() => {
      sync();
    }, moment.duration(interval).asMilliseconds());
  });
}

function syncGroups() {
  let groups = _.map(zoteroConfig.groups, 'id');
  // groups.push(2276160);
  return Promise.map(groups, syncGroup, {concurrency: 1});
}

function syncGroup(groupId) {
  let localVersion;
  let lastModifiedVersion;
  let createdItemsCount = 0;
  let updatedItemsCount = 0;
  let removedItemsCount = 0;
  // get local version
  return Sync.getCurrentVersion(groupId)
    .then(result => {
      localVersion = result;
      // get remote version changes
      let path = `${zoteroConfig.zoteroDomain}/groups/${groupId}/items`;
      return request({
        method: 'GET',
        uri: path,
        qs: {
          since: localVersion,
          format: 'versions'
        },
        headers: {
          'If-Modified-Since-Version': localVersion
        },
        json: true,
        resolveWithFullResponse: true
      })
      .then(response => {
        lastModifiedVersion = response.headers['last-modified-version'];
        let remoteVersions = _.map(response.body, (remoteVersion, itemKey) => {
          return { remoteVersion, itemKey };
        });
        // iterate remote versions
        return Promise.map(remoteVersions, ({remoteVersion, itemKey}) => {
          // get local item
          return Item.findOne({key: itemKey})
            .then(localItem => {
              // check if has updates
              if(localItem) {
                if(localItem.version < remoteVersion) {
                  updatedItemsCount++;
                  // get remote updated item
                  return request({
                    method: 'GET',
                    uri: `${path}/${itemKey}`,
                    json: true,
                  })
                  .then(updatedItem => {
                    // remove old item
                    return Item.removeItem(localItem)()
                      // add updated item
                      .then(Item.processItemAndSave(updatedItem));
                  });
                }
              }
              // is a new item
              else {
                createdItemsCount++;
                // get remote updated item
                return request({
                  method: 'GET',
                  uri: `${path}/${itemKey}`,
                  json: true,
                })
                .then(Item.processItemAndSave());
              }
            });
        }, {concurrency: 1});
      });
    })
    .then(() => {
      // Handler deletions
      let path = `${zoteroConfig.zoteroDomain}/groups/${groupId}/deleted?since=${localVersion}`;
      return request({
        method: 'GET',
        uri: path,
        json: true,
        resolveWithFullResponse: true
      })
      .then(response => {
        let keysToRemove = response.body.items;
        removedItemsCount = keysToRemove.length;
        if(removedItemsCount) {
          return Promise.map(keysToRemove, keyToRemove => {
            return Item.findOne({key: keyToRemove})
              .then(Item.removeItem());
          });
        }
      });
    })
    .then(() => {
      return Sync.setCurrentVersion(groupId, lastModifiedVersion);
    })
    .then(() => {
      return {
        group: groupId,
        created: createdItemsCount,
        modified: updatedItemsCount,
        removed: removedItemsCount
      };
    })
    .catch(err => {
      if(err.statusCode === 304) {
        return {group: groupId, nothingNew: true};
      } else if(err && err.cause && err.cause.code === 'ENOTFOUND') {
        console.log('ERR: ENOTFOUND');
        return 'ENOTFOUND';
      } else {
        return err;
      }
    });
}