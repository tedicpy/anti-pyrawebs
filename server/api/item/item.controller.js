/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/item              ->  index
 * POST    /api/item              ->  create
 * GET     /api/item/:id          ->  show
 * PUT     /api/item/:id          ->  upsert
 * PATCH   /api/item/:id          ->  patch
 * DELETE  /api/item/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Promise from 'bluebird';
import moment from 'moment';
import _ from 'lodash';
import { iterateAllPagesByGroup, zoteroConfig } from '../../components/zotero';
import config from '../../config/environment';
import Item from './item.model';
import request from 'request-promise';


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.error(err);
    res.status(statusCode).send(err);
  };
}


export function getFile(req, res) {
  let attachmentKey = req.params.key;
  return Item.findOne({key: attachmentKey})
    .then(attachmentItem => {
      let path = `${zoteroConfig.zoteroDomain}/groups/${attachmentItem.group}/items/${attachmentKey}/file`;
      return request({
        method: 'get',
        uri: path,
        encoding: null
      })
      .pipe( res );
    })
    .catch(handleError(res));
}

// Gets a single Item from the DB
export function showByKey(req, res) {
  return Item.findOne({key: req.params.key}).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


export function rebuild(req, res) {
  return Item.seedIfEmpty()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Items
export function index(req, res) {
  return Item.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function search(req, res) {
  let query = {};
  if(req.body.group) {
    query.group = req.body.group.toString();
  }
  if(req.body.tagsAny && _.isString(req.body.tagsAny)) {
    req.body.tagsAny = JSON.parse(req.body.tagsAny);
  }
  if(req.body.tagsAll && _.isString(req.body.tagsAll)) {
    req.body.tagsAll = JSON.parse(req.body.tagsAll);
  }
  if(req.body.tagsAny) {
    query.tags = {$in: req.body.tagsAny};
  }
  if(req.body.tagsAll) {
    query.tags = {$all: req.body.tagsAll};
  }
  if(req.body.source) {
    query.source = req.body.source;
  }
  if(req.body.year) {
    query.accessDate = {
      $gt: moment().year(req.body.year).startOf('year').subtract(1, 'seconds'),
      $lt: moment().year(req.body.year).endOf('year').add(1, 'seconds')
    };
  }
  let options = {
    page: parseInt(req.body.page) || 1,
    limit: parseInt(req.body.limit) || 10,
    sort: req.body.sort
  };
  query._type = {$ne: 'attachment'};
  return Item.paginate(query, options)
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Item in the DB
// export function create(req, res) {
//   return Item.create(req.body)
//     .then(respondWithResult(res, 201))
//     .catch(handleError(res));
// }

// Upserts the given Item in the DB at the specified ID
// export function upsert(req, res) {
//   if(req.body._id) {
//     Reflect.deleteProperty(req.body, '_id');
//   }
//   return Item.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

//     .then(respondWithResult(res))
//     .catch(handleError(res));
// }

// Updates an existing Item in the DB
// export function patch(req, res) {
//   if(req.body._id) {
//     Reflect.deleteProperty(req.body, '_id');
//   }
//   return Item.findById(req.params.id).exec()
//     .then(handleEntityNotFound(res))
//     .then(patchUpdates(req.body))
//     .then(respondWithResult(res))
//     .catch(handleError(res));
// }

// Deletes a Item from the DB
// export function destroy(req, res) {
//   return Item.findById(req.params.id).exec()
//     .then(handleEntityNotFound(res))
//     .then(removeEntity(res))
//     .catch(handleError(res));
// }
