import angular from 'angular';
import MobileDetect from 'mobile-detect';

let isMobile = (new MobileDetect(window.navigator.userAgent)).mobile();

export default class MenuItemComponent {
  /*@ngInject*/
  constructor(zotero, $element) {
    this.zotero = zotero;
    this.$element = $element;
    this.count = 0;
    this.preSelectCount = 0;
  }

  $onInit() {
    this.sidebar.setItemElement(this, this.$element);
  }

  getPrev() {
    if(isMobile) {
      return 0;
    }
    if(this.sidebar.preSelectShow) {
      if(this.preSelectCount >= this.count) {
        return this.preSelectCount;
      } else {
        return this.preSelectCount;
      }
    }
    return 0;
  }

  getCount() {
    if(isMobile) {
      return this.count;
    }
    if(this.sidebar.preSelectShow) {
      if(this.preSelectCount >= this.count) {
        return 0;
      } else {
        return this.count - this.preSelectCount;
      }
    }
    return this.count;
  }

  getPercent() {
    if(this.sidebar.preSelectShow) {
      if(this.preSelectCount) {
        return this.preSelectCount / this.sidebar.totalCountByTypePreview[this.type] * 100;
      }
      return 0;
    }
    return this.count / this.sidebar.totalCountByType[this.type] * 100;
  }
}
