'use strict';

export function extractDomain(url) {
  var domain;
  //find & remove protocol (http, ftp, etc.) and get domain
  if(url.indexOf('://') > -1) {
    domain = url.split('/')[2];
  }
  else {
    domain = url.split('/')[0];
  }

  //find & remove www
  if(domain.indexOf('www.') > -1) {
    domain = domain.split('www.')[1];
  }

  domain = domain.split(':')[0]; //find & remove port number
  domain = domain.split('?')[0]; //find & remove url params

  return domain;
}

/**
 * https://mongoosejs.com/docs/api.html#query_Query-cursor
 */
export function documentStream(query, process) {
  let cursor = query.cursor();
  return iterate();

  function iterate() {
    return cursor.next().then(data => {
      if(!data) {
        return;
      }
      let result = process(data);
      if(result && result.then) {
        return result.then(iterate);
      }
      return iterate();
    });
  }
}
