import angular from 'angular';

export class ListComponent {
  /*@ngInject*/
  constructor(zotero, Util, $timeout) {
    this.zotero = zotero;
    this.Util = Util;
    this.$timeout = $timeout;
  }

  copyItemLink(itemLink, copyItemLinkPopoverControls) {
    this.Util.copyTextToClipboard(itemLink);
    copyItemLinkPopoverControls.showDoneTooltip = true;
    this.$timeout(() => {
      copyItemLinkPopoverControls.showDoneTooltip = false;
    }, 5000);
  }
}

export default angular.module('antiPyrawebsApp.list', [])
  .component('list', {
    template: require('./list.html'),
    controller: ListComponent,
    controllerAs: 'vm'
  })
  .run(function ($templateCache) {
    'ngInject';
    $templateCache.put('item', require('./item.html'));
  })
  .name;
