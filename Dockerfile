# Usa una imagen de Ubuntu 18.04 como base
FROM ubuntu:18.04

MAINTAINER Lu Pa <admin@tedic.org>

# Establece variables de entorno
ENV DEBIAN_FRONTEND noninteractive
ENV NVM_DIR /root/.nvm
ENV CODE /usr/src/app

# Instala dependencias necesarias
RUN apt-get update && \
    apt-get install -y curl python2.7 python-pip && \
    apt-get clean

# Instala NVM (Node Version Manager)
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

# Configura NVM y Node.js
RUN . $NVM_DIR/nvm.sh && \
    nvm install 10 && \
    nvm alias default 10

# Agrega el directorio de binarios de NVM al PATH
ENV PATH $NVM_DIR/versions/node/v10.24.1/bin:$PATH

# Establece el directorio de trabajo
WORKDIR $CODE

# Copia los archivos de la aplicación
COPY . .

# Establece el PATH para los módulos de Node.js
RUN export PATH="$PATH:$HOME/node_modules/.bin"

# Copia el script de entrada del contenedor
COPY docker-entrypoint.sh /

# Instala pip2
RUN curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py && \
    python2.7 get-pip.py

# Establece el punto de entrada del contenedor
ENTRYPOINT ["/docker-entrypoint.sh"]

# Define el comando predeterminado para ejecutar la aplicación
CMD ["sh", "-c", "gulp serve:dist"]
