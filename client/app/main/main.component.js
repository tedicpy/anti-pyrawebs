import angular from 'angular';
import routing from './main.routes';

export class MainController {
  /*@ngInject*/
  constructor(zotero, $stateParams, scrollTo, $timeout) {
    this.zotero = zotero;
    this.$stateParams = $stateParams;
    this.scrollTo = scrollTo;
    this.$timeout = $timeout;
  }

  $onInit() {
    this.zotero.clear();
    if(this.$stateParams.clearFilters) {
      this.zotero.clearFilters();
    }
    if(this.$stateParams.itemKey) {
      this.zotero.showItem(this.$stateParams.itemKey);
      this.$timeout(() => {
        this.scrollTo('#list');
      }, 800);
    } else {
      this.zotero
        .setSelectedGroupByName(this.$stateParams.group)
        .setSelectedTagById(this.$stateParams.tag)
        .setSelectedYear(this.$stateParams.year)
        .getPage(1);
    }
  }
}

export default angular.module('antiPyrawebsApp.main', ['antiPyrawebsApp.zotero'])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController,
    controllerAs: 'vm'
  })
  .name;
