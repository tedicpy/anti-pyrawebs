import angular from 'angular';
import MenuItemComponent from './menuItem/menuItem';
import SidebarComponent from './sidebar/sidebar';

export default angular.module('antiPyrawebsApp.sidebar', [])
  .component('sidebar', {
    template: require('./sidebar/sidebar.html'),
    bindings: {
      viewType: '='
    },
    controller: SidebarComponent,
    controllerAs: 'vm'
  })
  .component('menuItem', {
    template: require('./menuItem/menuItem.html'),
    require: {
      sidebar: '^sidebar'
    },
    bindings: {
      item: '=',
      type: '@'
    },
    controller: MenuItemComponent,
    controllerAs: 'vm'
  })
  .name;