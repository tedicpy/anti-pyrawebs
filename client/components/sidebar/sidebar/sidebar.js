import slugify from 'slugify';
import $ from 'jquery';

export default class SidebarComponent {
  /*@ngInject*/
  constructor(zotero, $state, $http, $window, $timeout, $location, Util, $scope) {
    this.zotero = zotero;
    this.zotero.sidebar = this;
    this.$state = $state;
    this.$http = $http;
    this.$window = $window;
    this.$timeout = $timeout;
    this.$location = $location;
    this.Util = Util;
    this.menuItems = {};
    this.menuItemsByType = {group: [], tag: [], year: []};
    this.viewType = this.viewType || 'list';
    this.feedTooltipIsOpen = false;
    this.resetCount();
    this.resetPreSelect();
    $scope.$on('$stateChangeSuccess', (e, current) => {
      if(current.name === 'dashboard') {
        this.viewType = 'dashboard';
      } else {
        this.viewType = 'list';
      }
      this.setMenuHeight();
      this.getInitState();
    });
  }

  resetCount() {
    this.maxCountByType = {
      group: 0,
      tag: 0,
      year: 0
    };
    this.totalCountByType = {
      group: 0,
      tag: 0,
      year: 0
    };
  }

  resetPreSelect() {
    this.preSelectShow = false;
    // console.log('RESET fn');
    this.maxCountByTypePreview = {
      group: 0,
      tag: 0,
      year: 0
    };
    this.totalCountByTypePreview = {
      group: 0,
      tag: 0,
      year: 0
    };
  }

  getTotalCountByType(type) {
    if(this.preSelectShow) {
      return this.totalCountByTypePreview[type];
    }
    return this.totalCountByType[type];
  }

  setItemElement(menuItem) {
    this.menuItems[menuItem.item.id] = menuItem;
    this.menuItemsByType[menuItem.type].push(menuItem);
  }

  setPreSelect(item, type) {
    this.resetPreSelect();
    let preSelect = {};
    preSelect[type] = item;
    let query = this.getQueryArray(preSelect);
    // console.log('PRESELECT query', query);
    return this.$http.post('/api/viz/search', query).then(result => {
      if(item.previewCanceled) {
        // console.log('item.previewCanceled', item.previewCanceled);
      } else {
        this.preSelectShow = true;
        // console.log('PRESELECT result.data', result.data);
        this.assignPreSelectCount(result.data);
      }
    });
  }

  setPreSelectWithDelay(item, type) {
    item.previewCanceled = false;
    let timeoutPromise = this.$timeout(() => {
      Reflect.deleteProperty(item, 'timeoutPromise');
      this.setPreSelect(item, type);
    }, 500);
    item.timeoutPromise = timeoutPromise;
  }

  clearItemDelay(item) {
    item.previewCanceled = true;
    if(item.timeoutPromise) {
      this.$timeout.cancel(item.timeoutPromise);
      Reflect.deleteProperty(item, 'timeoutPromise');
    }
  }

  setMenuHeight() {
    this.$timeout(() => {
      $('.menu-item-container').each((idx, el) => {
        let $el = $(el);
        $el.css({ height: $el.find('menu-item').length * 35 });
      });
    });
  }

  getInitState() {
    let query = this.getQueryArray();
    // console.log('INIT query', query);
    this.$http.post('/api/viz/search', query).then(result => {
      // console.log('INIT result.data', result.data);
      this.assignCount(result.data);
      this.reorder();
    });
  }

  reorder() {
    this.$window.clearTimeout(this.timer);
    this.timer = this.$window.setTimeout(() => {
      this.runReorder('group');
      this.runReorder('tag');
      this.runReorder('year');
    }, 300);
  }

  runReorder(type) {
    if (type !== 'year'){
      this.menuItemsByType[type].sort((a, b) => {
        return b.count - a.count;
      });
    }
    _.forEach(this.menuItemsByType[type], (item, index) => {
      let newTop = index * 35;
      let $el = $(item.$element);
      if(newTop != parseInt($el.css('top'))) {
        $el.css({
          top: newTop
        })
        .one('webkitTransitionEnd', () => {
          $el.removeClass('moving');
        })
        .addClass('moving');
      }
    });
  }

  assignCount(countArrayResult) {
    this.resetCount();
    _.forEach(countArrayResult, countItem => {
      if(countItem.result && countItem.result.length) {
        let menuItem = this.menuItems[countItem.key];
        menuItem.count = countItem.result[0].count;
        this.totalCountByType[menuItem.type] += menuItem.count;
        if(menuItem.count > this.maxCountByType[menuItem.type]) {
          this.maxCountByType[menuItem.type] = menuItem.count;
        }
      } else {
        this.menuItems[countItem.key].count = 0;
      }
    });
  }

  assignPreSelectCount(countArrayResult) {
    this.maxCountByTypePreview = _.clone(this.maxCountByType);
    _.forEach(countArrayResult, countItem => {
      if(countItem.result && countItem.result.length) {
        let menuItem = this.menuItems[countItem.key];
        menuItem.preSelectCount = countItem.result[0].count;
        this.totalCountByTypePreview[menuItem.type] += menuItem.preSelectCount;
        if(menuItem.preSelectCount > this.maxCountByTypePreview[menuItem.type]) {
          this.maxCountByTypePreview[menuItem.type] = menuItem.preSelectCount;
        }
      } else {
        this.menuItems[countItem.key].preSelectCount = 0;
      }
    });
  }

  getQueryArray(preSelect = {}) {
    let queryArray = [];
    if(_.size(preSelect)) {
      preSelect.show = true;
    }
    _.forEach(this.zotero.config.groups, group => {
      queryArray.push( this.getItemQuery(group, 'group', preSelect) );
    });
    _.forEach(this.zotero.config.tags, tag => {
      queryArray.push( this.getItemQuery(tag, 'tag', preSelect) );
    });
    _.forEach(this.zotero.getYears(), year => {
      queryArray.push( this.getItemQuery(year, 'year', preSelect) );
    });
    return queryArray;
  }

  getItemQuery(item, type, preSelect = {}) {
    let query = {
      key: item.id
    };

    if(type === 'source') {
      query.source = item.id;
      // otros filtros
      this.addTagsToQuery(query, preSelect.tag);
      this.addGroupToQuery(preSelect, query);
      this.addYearToQuery(preSelect, query);
    }

    if(type === 'group') {
      if(preSelect.group) {
        if(preSelect.group.id === item.id) {
          query.group = preSelect.group.id;
          // otros filtros
          this.addYearToQuery(preSelect, query);
          this.addTagsToQuery(query, preSelect.tag);
        }
      } else {
        if(this.zotero.selectedGroup) {
          if(this.zotero.selectedGroup.id === item.id) {
            query.group = this.zotero.selectedGroup.id;
            // otros filtros
            this.addYearToQuery(preSelect, query);
            this.addTagsToQuery(query, preSelect.tag);
          }
        } else {
          query.group = item.id;
          // otros filtros
          this.addYearToQuery(preSelect, query);
          this.addTagsToQuery(query, preSelect.tag);
        }
      }
    }

    if(type === 'tag') {
      if(preSelect.tag) {
        if(preSelect.tag.id === item.id) {
          this.addTagsToQuery(query, preSelect.tag);
          // otros filtros
          this.addGroupToQuery(preSelect, query);
          this.addYearToQuery(preSelect, query);
        } else {
          this.addTagsToQuery(query, item);
          this.addTagsToQuery(query, preSelect.tag);
          // otros filtros
          this.addGroupToQuery(preSelect, query);
          this.addYearToQuery(preSelect, query);
        }
      } else {
        this.addTagsToQuery(query, item);
        // otros filtros
        this.addGroupToQuery(preSelect, query);
        this.addYearToQuery(preSelect, query);
      }
    }

    if(type === 'year') {
      if(preSelect.year) {
        if(preSelect.year.id === item.id) {
          query.date = preSelect.year.name;
          query.sumAll = true;
          // otros filtros
          this.addGroupToQuery(preSelect, query);
          this.addTagsToQuery(query, preSelect.tag);
        }
      } else {
        if(this.zotero.selectedYear) {
          if(this.zotero.selectedYear.id === item.id) {
            query.date = this.zotero.selectedYear.name;
            query.sumAll = true;
            // otros filtros
            this.addGroupToQuery(preSelect, query);
            this.addTagsToQuery(query, preSelect.tag);
          }
        } else {
          query.date = item.name;
          query.sumAll = true;
          // otros filtros
          this.addGroupToQuery(preSelect, query);
          this.addTagsToQuery(query, preSelect.tag);
        }
      }
    }

    return query;
  }

  addTagsToQuery(query, item) {
    let toAdd = [];
    _.forEach(this.zotero.selectedTags, tag => {
      if(tag) {
        toAdd.push( slugify(tag.id, {lower: true}) );
      }
    });
    if(item && !_.includes(this.zotero.selectedTags, item)) {
      toAdd.push( slugify(item.id, {lower: true}) );
    }
    if(toAdd.length) {
      query.tagsAll = query.tagsAll || [];
      query.tagsAll = _.union(query.tagsAll, toAdd);
      query.multiTag = true;
      query.sumAll = true;
    }
  }

  addGroupToQuery(preSelect, query) {
    if(this.zotero.selectedGroup) {
      query.group = this.zotero.selectedGroup.id;
    }
    if(preSelect.group) {
      query.group = preSelect.group.id;
    }
  }

  addYearToQuery(preSelect, query) {
    if(this.zotero.selectedYear) {
      query.date = this.zotero.selectedYear.name;
      query.sumAll = true;
    }
    if(preSelect.year) {
      query.date = preSelect.year.name;
      query.sumAll = true;
    }
  }

  goToGroup(group) {
    if(this.zotero.selectedGroup !== group) {
      this.zotero.selectedGroup = group;
      if(this.viewType === 'list') {
        this.$state.go('main.group', { group: group.name });
      }
    } else {
      this.zotero.selectedGroup = null;
      if(this.viewType === 'list') {
        this.$state.go('main', {});
      }
    }
    this.getInitState();
    this.zotero.getPage(1);
    this.zotero.selectedItem = null;
    if(this.viewType === 'dashboard') {
      this.zotero.dashboard.getInitState();
    }
  }

  goToTag(tag) {
    if(this.zotero.selectedTags[tag.id] !== tag) {
      this.zotero.selectedTags[tag.id] = tag;
      if(this.viewType === 'list') {
        this.$state.go('main.tag', { tag: tag.id });
      }
    } else {
      Reflect.deleteProperty(this.zotero.selectedTags, tag.id);
      if(this.viewType === 'list') {
        this.$state.go('main', { });
      }
    }
    this.getInitState();
    this.zotero.getPage(1);
    this.zotero.selectedItem = null;
    if(this.viewType === 'dashboard') {
      this.zotero.dashboard.getInitState();
    }
  }

  goToYear(year) {
    if(this.zotero.selectedYear !== year) {
      this.zotero.selectedYear = year;
      if(this.viewType === 'list') {
        this.$state.go('main.year', { year: year.name });
      }
    } else {
      this.zotero.selectedYear = null;
      if(this.viewType === 'list') {
        this.$state.go('main', { });
      }
    }
    this.getInitState();
    this.zotero.getPage(1);
    this.zotero.selectedItem = null;
    if(this.viewType === 'dashboard') {
      this.zotero.dashboard.getInitState();
    }
  }

  clearFilters() {
    this.zotero.selectedGroup = null;
    _.forEach(this.zotero.selectedTags, tag => {
      if(tag) {
        Reflect.deleteProperty(this.zotero.selectedTags, tag.id);
      }
    });
    this.zotero.selectedYear = null;
    this.zotero.selectedItem = null;
    this.getInitState();
    this.zotero.getPage(1);
    if(this.viewType === 'list') {
      this.$state.go('main.clear', { });
    }
    if(this.viewType === 'dashboard') {
      this.zotero.dashboard.getInitState();
    }
  }

  copyFeedLink(feedType) {
    let feedLink = this.zotero.generateFeedLink(feedType);
    this.Util.copyTextToClipboard(feedLink);
    this.feedTooltip = `Enlace del feed ${feedType} copiado al portapapeles`;
    this.feedTooltipIsOpen = true;
    if(this.feedTooltipTimeout) {
      this.$timeout.cancel(this.feedTooltipTimeout);
    }
    this.feedTooltipTimeout = this.$timeout(() => {
      this.feedTooltipIsOpen = false;
    }, 5000);
  }
}
