import angular from 'angular';
import $ from 'jquery';

export default angular.module('antiPyrawebsApp.header', [])
  .controller('headerBloque1Ctrl', ['$window', '$timeout', '$scope', function($window, $timeout, $scope) {
    let $win = $($window); // wrap window object as jQuery object
    let $nota1Background = $('#nota1-background');
    let $nota1Content = $('#nota1-content');
    $win.on('resize', onResize);
    $scope.init = () => {
      repeat();
      let i = 10;
      function repeat() {
        $timeout(() => {
          onResize();
          if(i--) {
            repeat();
          }
        }, 300);
      }
    };
    function onResize() {
      let contentHeight = $nota1Content.css('height');
      $nota1Background.css('height', contentHeight);
      $nota1Content.css({
        top: `-${contentHeight}`,
        marginBottom: `-${contentHeight}`,
      });
    }
  }])
  .directive('fixedTop', ['$window', function($window) {
    var $win = $($window); // wrap window object as jQuery object
    return {
      restrict: 'A',
      link(scope, element, attrs) {
        let $el = $(element);
        $win.on('scroll', () => {
          if($win.scrollTop() >= 300) {
            $el.fadeIn(300);
          } else {
            $el.fadeOut(300);
          }
        });
      }
    };
  }])
  .directive('scrollTo', ['scrollTo', function(scrollTo) {
    return {
      restrict: 'A',
      link(scope, element, attr) {
        let $el = $(element);
        let handler = () => {
          scrollTo(attr.scrollTo);
        };
        $el.bind('click', handler);
        scope.$on('$destroy', () => {
          $el.unbind('click', handler);
        });
      }
    };
  }])
  .factory('scrollTo', function() {
    return function (scrollTo) {
      $('html, body').animate({
        scrollTop: $(scrollTo).offset().top - 70
      }, 1000);
    };
  })
  .run(['$templateCache', function($templateCache) {
    $templateCache.put('header', require('./header.html'));
    $templateCache.put('menu-top', require('./menu-top.html'));
    $templateCache.put('bloque1', require('./bloque1.html'));
    $templateCache.put('bloque2', require('./bloque2.html'));
  }])
  .name;
