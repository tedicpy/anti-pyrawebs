'use strict';

import main from './main.component';
import {
  MainController
} from './main.component';

describe('Component: MainComponent', function() {
  beforeEach(angular.mock.module(main));
  beforeEach(angular.mock.module('stateMock'));

  var scope;
  var mainComponent;
  var state;
  var $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController, zotero) {
    mainComponent = $componentController('main', {
      zotero
    });
  }));

  it('should attach a list of things to the controller', function() {
    mainComponent.$onInit();
    expect(mainComponent.pages.length)
      .to.equal(1);
  });
});
