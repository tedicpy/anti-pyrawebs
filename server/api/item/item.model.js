'use strict';

import mongoose from 'mongoose';
import _ from 'lodash';
import slugify from 'slugify';
import Promise from 'bluebird';
import mongoosePaginate from 'mongoose-paginate-v2';
import { extractDomain } from '../../components/utils';
import { iterateAllPagesByGroup, zoteroConfig } from '../../components/zotero';

var ItemSchema = new mongoose.Schema({
  key: {
    type: String
  },
  _type: { // 'attachment', 'webpage', 'blogPost'
    type: String
  },
  group: {
    type: String
  },
  parent: {
    type: String
  },
  attachments: [String],
  source: {
    type: String
  },
  accessDate: {
    type: Date
  },
  tags: {
    type: [String]
  },
  version: {
    type: Number
  },
  links: {
    type: Object
  },
  meta: {
    type: Object
  },
  data: {
    type: Object
  }
});

ItemSchema.index({ key: -1 });
ItemSchema.index({ key: -1, _type: 1 });
ItemSchema.index({ accessDate: -1 });
ItemSchema.index({ group: 1, accessDate: -1 });
ItemSchema.index({ tags: 1, accessDate: -1 });
ItemSchema.index({ source: 1, accessDate: -1 });
ItemSchema.index({ group: 1, tags: 1, source: 1, accessDate: -1 });

ItemSchema.plugin(mongoosePaginate);

ItemSchema.statics.formatItem = function(item) {
  item.group = item.library.id;
  item.accessDate = item.data.accessDate;
  item._type = item.data.itemType;
  item.parent = item.data.parentItem;
  if(item.data.url) {
    item.source = extractDomain(item.data.url);
  }
  item.tags = _.map(item.data.tags, tagObj => slugify(tagObj.tag, {lower: true}));
  return item;
};

ItemSchema.statics.seedIfEmpty = function() {
  let Item = this;
  let limitPage = null; // traer todas las páginas
  let itemsPerPage = 50;
  let itemsCount = 0;
  return Item.countDocuments().then(count => {
    if(!count) {
      return Promise.map(zoteroConfig.groups, group => {
        return iterateAllPagesByGroup(group.id, items => {
          return Promise.map(items, item => {
            ++itemsCount;
            return Item.processItemAndSave(item);
          });
        }, limitPage, itemsPerPage);
      })
      .then(() => itemsCount);
    }
  });
};

/**
 * si se agrega un nuevo
 *  itemPost
 *    agregar al pivot
 *    generar itemPost.attachments[] (en caso de haberse actualizado)
 *  itemAttachment
 *    $addToSet attachmentKey to parentItem.attachments[]
 */
ItemSchema.statics.processItemAndSave = function(specificItem) {
  let Item = this;
  let Pivot = mongoose.model('Pivot');
  return returnedItem => {
    let newItem = specificItem || returnedItem;
    let localItem;
    Item.formatItem(newItem);
    // add new item
    return Item.create(newItem)
      .then(createdItem => {
        localItem = createdItem;
        // si es attachment
          // agregar attachmentKey a parentItem.attachments[]
        if(localItem._type === 'attachment') {
          return Item.update({
            key: localItem.parent
          }, {
            $addToSet: {
              attachments: localItem.key
            }
          });
        }
        // si es un itemPost
        else {
          // agregar al pivot
          return Pivot.add(localItem)
            .then(() => {
              return Item.find({
                parent: localItem.key
              }, 'key');
            })
            // generar itemPost.attachments[] (en caso de haberse actualizado)
            .then(attachments => {
              let attachmentsKeys = _.map(attachments, 'key');
              return Item.update({
                key: localItem.key
              }, {
                $set: {
                  attachments: _.map(attachments, 'key')
                }
              });
            });
        }
      })
      .then(() => localItem);
  };
};

/**
 * si se elimino un
 *  itemPost
 *    eliminar itemPost
 *  itemAttachment
 *    eliminar attachmentKey de parentItem.attachments[]
 *    eliminar itemAttachment
 */
ItemSchema.statics.removeItem = function(specificItem) {
  let Item = this;
  let Pivot = mongoose.model('Pivot');
  return returnedItem => {
    let itemToRemove = returnedItem || specificItem;
    if(!itemToRemove) {
      return Promise.resolve();
    }
    if(itemToRemove._type === 'attachment') {
       // eliminar attachmentKey de parentItem.attachments[]
      return Item.update({
        key: itemToRemove.parent
      }, {
        $pull: {
          attachments: itemToRemove.key
        }
      })
      .then(() => {
        return itemToRemove.remove();
      });
    }
    else {
      return Pivot.remove(itemToRemove)
        .then(() => {
          return itemToRemove.remove();
        });
    }
  };
};

export default mongoose.model('Item', ItemSchema);
