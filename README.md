# Observatorio Antipyrawebs

Esta herramienta ha sido desarrollada en conjunto con [WebLab](https://www.weblab.com.py).

## Getting Started

This project was generated with the [Angular Full-Stack Generator](https://github.com/DaftMonk/generator-angular-fullstack) version 4.2.3.

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node >= 4.x.x, npm >= 2.x.x
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [MongoDB](https://www.mongodb.org/) - Keep a running daemon with `mongod`

### Developing

1. Run `npm install` to install server dependencies.

2. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running

3. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Run `gulp build` for building and `gulp serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.

## With docker

**Dependences:** You need `docker` and `docker-compose` installed

Build and up (first time):
`docker compose up -d`

If you change something, then build, down and up again:
`docker compose build && docker compose down && docker compose up -d`

**Config file:** check `server/config/environment/development.js` and set db connection: `mongodb://db`

### Production

Para hacer correr en modo producción, son necesarios los siguientes cambios
 - `docker compose.yml`: reemplazar puertos `3000` por `8080`
 - `Dockerfile`: reemplazar `gulp serve` por `gulp serve:dist`
