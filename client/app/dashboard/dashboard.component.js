import angular from 'angular';
import appConfig from '../../../server/config/environment/shared';
import echarts from 'echarts';
import _ from 'lodash';
import slugify from 'slugify';
import $ from 'jquery';

let colorList = ['#FEB83E', '#17988E', '#FA6726', '#48F0E5'];

export class DashboardController {
  constructor(zotero, $http) {
    'ngInject';
    this.zotero = zotero;
    this.$http = $http;
    this.zotero.dashboard = this;
    this.resetTotalCount();
  }

  $onInit() {
    this.chart = {
      pie: echarts.init(document.getElementById('pie')),
      bar: echarts.init(document.getElementById('bar')),
      barSource: echarts.init(document.getElementById('bar-source')),
      radar: echarts.init(document.getElementById('radar')),
      calendar: echarts.init(document.getElementById('calendar'))
    };
    $(window).on('resize', () => {
      _.forEach(this.chart, chart => chart.resize());
    });
    this.getInitState();
  }

  resetTotalCount() {
    this.statistics = {
      pie: [],
      bar: {},
      barSource: {},
      radar: {},
      calendar: {}
    };
  }

  getInitState(){
    this.pieData();
    this.barData();
    this.barFuenteData();
    this.radarData();
    this.calendarData();
  }

  pieData() {
    var queries = [];
    this.statistics.pie = [];
    _.map(appConfig.groups, group => {
      var qs = this.zotero.sidebar.getItemQuery(group, 'group');
      qs.key = group.name;
      return queries.push(qs)
    });
    this.$http.post('/api/viz/search', queries).then(result => {
      result.data.forEach(element => {
        var count = 0;
        if (element.result[0]){
          count = element.result[0].count;
        }
        if (count > 0 ){
          this.statistics.pie.push({
            name: element.key,
            value: count
          });
        }
      });
      // console.log("this.statistics.pie", this.statistics.pie);
      this.pieChart();
    });
  }

  pieChart(){
    this.chart.pie.setOption({
        title: {
            text: "Artículos por país"
        },
        tooltip : {
          trigger: 'item',
          formatter: "{b} : {c} ({d}%)"
        },
        series : [
            {
                name: 'Reference Page',
                type: 'pie',
                radius: '55%',
                data:this.statistics.pie
            }
        ],
        color: colorList
    });
  }

  barData(){
    this.statistics.bar.xAxis = []
    this.statistics.bar.serie = []
    var queries = [];

    appConfig.tags.forEach(tag => {
      this.statistics.bar.xAxis.push(tag.name)
      var qs = this.zotero.sidebar.getItemQuery(tag, 'tag');
      qs.key = tag.name;
      queries.push(qs);
    });

    this.$http.post('/api/viz/search', queries).then(result => {
      result.data.forEach(element => {
        var count = 0;
        if (element.result.length == 1){
          count = element.result[0].count
        }
        this.statistics.bar.serie.push(count)
      });
      // console.log("this.statistics.bar", this.statistics.bar);
      this.barChart();
    });
  }

  barChart(){
    var option = {
        title: {
            text: 'Artículos por tema'
        },
        tooltip: {},
        xAxis: {
            data: this.statistics.bar.xAxis,
            axisLabel:{
              rotate: 45
            },
        },
        grid: {
          bottom: 120
        },
        yAxis: {},
        series: [{
            name: 'Artículos',
            type: 'bar',
            data: this.statistics.bar.serie,
            label: {show: false}
        }],
        color: colorList
    };
    this.chart.bar.setOption(option);
  }

  barFuenteData(){
    this.$http.post('/api/viz/search', {sourceAll: true}).then(result => {
      _.remove(result.data, element => {
        return element.source === "NO-SOURCE";
      });

      let sources = result.data;
      let queryArray = _.map(sources, item => {
        return this.zotero.sidebar.getItemQuery({id: item.source}, 'source');
      });

      this.$http.post('/api/viz/search', queryArray).then(result => {
        var ordenado = _.orderBy(result.data, item => {
          var count = 0;
          if (item.result[0]){
            count = item.result[0].count
          }
          return count
        }, 'desc');
        ordenado = ordenado.slice(0, 10);
        ordenado = _.orderBy(ordenado, 'key', 'asc');

        this.statistics.barSource.xAxis = _.map(ordenado, element => {
          return element.key
        });

        this.statistics.barSource.serie = _.map(ordenado, element => {
          var count = 0;
          if (element.result.length > 0){
            count = element.result[0].count;
          }
          return count;
        });

        // console.log("this.statistics.barSource", this.statistics.barSource);
        this.barSourceChart();
      });
    });
  }

  barSourceChart(){
    var option = {
        title: {
            text: 'Artículos por fuente'
        },
        tooltip: {},
        xAxis: {
            data: this.statistics.barSource.xAxis,
            axisLabel:{
              rotate: 45
            },
        },
        grid: {
          bottom: 120
        },
        yAxis: {},
        series: [{
            name: 'Fuentes',
            type: 'bar',
            data: this.statistics.barSource.serie,
            label: {show: false}
        }],
        color: colorList
    };
    this.chart.barSource.setOption(option);
  }

  radarData(){
    this.statistics.radar.legends = [];
    this.statistics.radar.data = [];

    var queries = [];
    var groups = appConfig.groups;
    if (this.zotero.selectedGroup){
      groups = [this.zotero.selectedGroup];
    }
    var tags = appConfig.tags;
    if (this.zotero.selectedTags.length == 0){
      tags = [this.zotero.selectedTags];
    }
    groups.forEach(group => {
      this.statistics.radar.legends.push(group.name);
      tags.forEach(tag => {
        queries.push({
          key: {
            tagsAny: [tag.id],
            group: group.name
          },
          tagsAll: [slugify(tag.id, {lower: true})],
          multiTag: true,
          group: group.id,
          sumAll: true
        });
      });
    });

    this.$http.post('/api/viz/search', queries).then(result => {
      var groupPrev = '';
      var resultado = {};
      resultado.value = [];
      var resultados = [];
      _.map(result.data, element => {
        var count = 0;
        if (element.result.length > 0){
          count = element.result[0].count;
        }
        var group = element.key.group;
        if (group === groupPrev){
          resultado.name = group;
        }else if(groupPrev !== ''){
          resultados.push(resultado);
          resultado = {};
          resultado.value = [];
        }
        resultado.value.push(count);
        groupPrev = group;
        return resultado;
      });
      resultados.push(resultado);
      this.statistics.radar.data = resultados;

      //indicators: limites maximos para cada eje (tag)
      this.statistics.radar.indicators = [];
      for (var i=0; i < appConfig.tags.length; i++){
        var array = [];
        this.statistics.radar.data.forEach(element => {
          array.push(Number(element.value[i]));
        });
        this.statistics.radar.indicators.push({
          name: appConfig.tags[i].name,
          max: Number(_.max(array) + 5)
        });
      }
      // console.log("this.statistics.radar", this.statistics.radar);

      return this.radarChart(this.statistics.radar.indicators);
    });
  }

  radarChart(indicators){
    // radar chart. Fuente: https://ecomfe.github.io/echarts-examples/public/editor.html?c=radar
    var option = {
      title: {
          text: 'Por tema y País'
      },
      tooltip: {},
      legend: {
          data: this.statistics.radar.legends,
          top: 20,
          left: 'center'
      },
      radar: {
          // shape: 'circle',
          center: ['50%', '60%'],
          name: {
              textStyle: {
                  color: '#fff',
                  backgroundColor: '#999',
                  borderRadius: 3,
                  padding: [3, 5]
              }
          },
          indicator: this.statistics.radar.indicators
      },
      series: [{
          name: 'Por tema y País',
          type: 'radar',
          // areaStyle: {normal: {}},
          data : this.statistics.radar.data,
          top: 20
      }],
      color: colorList
    };
    this.chart.radar.setOption(option);
  }

  calendarData(){
    this.statistics.calendar.series = []
    var currentYear = (new Date()).getFullYear()
    var dates = [];
    var queries = [];
    var positions = [];
    var height = 180;
    var years = [];
    if (this.zotero.selectedYear){
      years.push(parseInt(this.zotero.selectedYear.id));
    } else {
      for (var i = 0; i < appConfig.lastYears; i++){
        years.push(currentYear-i);
      }
    }

    var i = 0;
    var previousPositionTop = 0;
    years.forEach(year => {
      var qs = this.zotero.sidebar.getItemQuery({
        id: year,
        name: String(year)
      }, 'year');
      qs.sumAll = false
      queries.push(qs);

      dates.push({
        date: String(year)
      });
      var position = {
        range: String(year),
        cellSize: ['auto', 20],
        itemStyle: {
          normal: {
              color: '#f2f1ef',
              borderWidth: 1,
              borderColor: '#c9c8c7'
          }
        },
        splitLine: {
          show: true,
          lineStyle: {
              color: '#000',
              width: 0.5,
              type: 'solid'
          }
        },
        dayLabel: {
          firstDay: 0,
          nameMap: ['D', 'L', 'M', 'M', 'J', 'S', 'D']
        },
        monthLabel: {
          nameMap: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        }
      };
      position.top = previousPositionTop + height;
      if (previousPositionTop == 0){
        position.top = 80
      }
      previousPositionTop = position.top
      positions.push(position);
      i++;
    });

    this.statistics.calendar.positions = positions;
    this.statistics.calendar.max = 0;
    var i = 0;
    var series = [];
    var max = 0;
    this.$http.post('/api/viz/search', queries).then(result => {
      result.data.forEach(yearDataFromApi => {
        var data = [];
        _.map(yearDataFromApi.result, element => {
          data.push([
            element.date,
            element.count
          ]);
        });
        var year = String(yearDataFromApi.key);
        var yearData = data; //this.getYearData(year, data);
        if (yearData.length > 0){
          max = _.maxBy(yearData, element => {
            return element[1]
          });
        }
        if (max){
          max = max[1]
          if (max > this.statistics.calendar.max){
            this.statistics.calendar.max = max
          }
        }

        series.push({
          type: 'heatmap',
          coordinateSystem: 'calendar',
          calendarIndex: _.indexOf(years, yearDataFromApi.key),
          data: yearData
        });
      });
      this.statistics.calendar.series = series
      // console.log("this.statistics.calendar", this.statistics.calendar);

      this.calendarChart();
    });
  }

  getYearData(year, data){
    // if (data.length == 0) return
    var date = +echarts.number.parseDate(year + '-01-01');
    var end = +echarts.number.parseDate((+year + 1) + '-01-01');
    var dayTime = 3600 * 24 * 1000;
    var newData = [];
    var count;
    for (var time = date; time < end; time += dayTime) {
      let dateString = echarts.format.formatTime('yyyy-MM-dd', time);
      count = 0;
      var index = data.findIndex(element => {
        return element[0] === dateString
      });
      if (index != -1){
        count = data[index][1]
      }
      newData.push([
        dateString,
        count
      ]);
    }
    return newData;
  }

  calendarChart(){
    var option = {
      title: {
        left: 'center',
        text: 'Cantidad de artículos'
      },
      tooltip: {
          position: ['20%', '20%'],
          formatter: function (p) {
            var format = echarts.format.formatTime('yyyy-MM-dd', p.data[0]);
            var marker = "<span style=\"display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:"+p.color+";\"></span>"
            return  format + ': ' + marker + p.data[1];
          }
      },
      visualMap: {
          min: 0,
          max: this.statistics.calendar.max,
          calculable: true,
          orient: 'horizontal',
          left: 'center',
          top: 20,
          inRange : {
            color: ['#FEB83E', '#fa6726' ]
          }
      },

      calendar: this.statistics.calendar.positions,

      series: this.statistics.calendar.series
    };
    this.chart.calendar.setOption(option, true);
  }
}

export default angular.module('antiPyrawebsApp.dashboard', ['antiPyrawebsApp.zotero'])
  .component('dashboard', {
    template: require('./dashboard.html'),
    controller: DashboardController,
    controllerAs: 'vm'
  })
  .name;
