'use strict';

export default function routes($stateProvider) {
  'ngInject';
  $stateProvider
    .state('main', {
      url: '/',
      template: '<main></main>'
    })
    .state('main.group', {
      url: 'group/:group'
    })
    .state('main.tag', {
      url: 'tag/:tag'
    })
    .state('main.year', {
      url: 'year/:year'
    })
    .state('main.item', {
      url: 'item/:itemKey'
    })
    .state('main.clear', {
      params: {
        clearFilters: true
      }
    })
}
