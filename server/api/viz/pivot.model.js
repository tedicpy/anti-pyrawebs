'use strict';

import mongoose from 'mongoose';
import _ from 'lodash';
import Promise from 'bluebird';
import moment from 'moment';
import { documentStream } from '../../components/utils';

var PivotSchema = new mongoose.Schema({
  group: {
    type: Number
  },
  source: {
    type: String
  },
  tags: {
    type: [String]
  },
  multiTag: {
    type: Boolean
  },
  date: {
    type: String // ej: '2018-12-02', '2016-07-28'
  },
  count: {
    type: Number
  }
});

PivotSchema.index({group: 1}); //
PivotSchema.index({tags: 1}); //all //single
PivotSchema.index({source: 1}); //
PivotSchema.index({date: 1}); //
PivotSchema.index({group: 1, tags: 1}); //all //single
PivotSchema.index({group: 1, source: 1}); //
PivotSchema.index({group: 1, date: 1}); //
PivotSchema.index({tags: 1, source: 1}); //all //single
PivotSchema.index({tags: 1, date: 1}); //all //single
PivotSchema.index({source: 1, date: 1}); //
PivotSchema.index({group: 1, tags: 1, source: 1}); //all //single
PivotSchema.index({group: 1, tags: 1, date: 1}); //all //single
PivotSchema.index({group: 1, date: 1, source: 1}); //
PivotSchema.index({date: 1, tags: 1, source: 1}); //all //single
PivotSchema.index({group: 1, tags: 1, source: 1, date: 1}); //all

PivotSchema.statics.rebuild = function() {
  let Pivot = this;
  let Item = mongoose.model('Item');
  return Pivot.find().remove()
    .then(() => {
      return documentStream(Item.find(), item => {
        return Pivot.add(item);
      })
    })
};

PivotSchema.statics.add = function(item) {
  let updates = combiner(item, {
    $inc: {count: 1}
  });
  return Promise.map(updates, update => {
    return this.update(update.query, update.modifier, {upsert: true});
  }, {concurrency: 1});
};

PivotSchema.statics.remove = function(item) {
  let updates = combiner(item, {
    $inc: {count: -1}
  });
  return Promise.map(updates, update => {
    return this.update(update.query, update.modifier, {upsert: true});
  }, {concurrency: 1});
};

function combiner(item, modifier) {
  let updates = [];
  let date = moment(item.accessDate).format('YYYY-MM-DD');
  item.tags = item.tags || [];
  item.source = item.source || 'NO-SOURCE';
  // group
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $exists: false },
      tags: { $exists: false },
      date: { $exists: false }
    },
    modifier
  });
  // source
  updates.push({
    query: {
      group: { $exists: false },
      source: { $eq: item.source },
      tags: { $exists: false },
      date: { $exists: false }
    },
    modifier
  });
  // date
  updates.push({
    query: {
      group: { $exists: false },
      source: { $exists: false },
      tags: { $exists: false },
      date: { $eq: date }
    },
    modifier
  });
  // group with date
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $exists: false },
      tags: { $exists: false },
      multiTag: { $exists: false },
      date: { $eq: date }
    },
    modifier
  });
  // group with source
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $eq: item.source },
      tags: { $exists: false },
      multiTag: { $exists: false },
      date: { $exists: false }
    },
    modifier
  });
  // source with date
  updates.push({
    query: {
      group: { $exists: false },
      source: { $eq: item.source },
      tags: { $exists: false },
      multiTag: { $exists: false },
      date: { $eq: date }
    },
    modifier
  });
  // each tag
  _.forEach(item.tags, tag => {
    // only tag
    updates.push({
      query: {
        group: { $exists: false },
        source: { $exists: false },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $exists: false }
      },
      modifier
    });
    // with group
    updates.push({
      query: {
        group: { $eq: item.group },
        source: { $exists: false },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $exists: false }
      },
      modifier
    });
    // with source
    updates.push({
      query: {
        group: { $exists: false },
        source: { $eq: item.source },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $exists: false }
      },
      modifier
    });
    // with date
    updates.push({
      query: {
        group: { $exists: false },
        source: { $exists: false },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $eq: date }
      },
      modifier
    });

    // single tag with group and source
    updates.push({
      query: {
        group: { $eq: item.group },
        source: { $eq: item.source },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $exists: false }
      },
      modifier
    });
    // single tag with group and date
    updates.push({
      query: {
        group: { $eq: item.group },
        source: { $exists: false },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $eq: date }
      },
      modifier
    });
    // single tag with source and date
    updates.push({
      query: {
        group: { $exists: false },
        source: { $eq: item.source },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $eq: date }
      },
      modifier
    });
    // single tags with group, source and date
    updates.push({
      query: {
        group: { $eq: item.group },
        source: { $eq: item.source },
        tags: { $eq: [tag] },
        multiTag: false,
        date: { $eq: date }
      },
      modifier
    });
  });
  // all tags
  updates.push({
    query: {
      group: { $exists: false },
      source: { $exists: false },
      tags: { $eq: item.tags },
      multiTag: true,
      date: { $exists: false }
    },
    modifier
  });
  // all tags with group
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $exists: false },
      tags: { $eq: item.tags },
      multiTag: true,
      date: { $exists: false }
    },
    modifier
  });
  // all tags with source
  updates.push({
    query: {
      group: { $exists: false },
      source: { $eq: item.source },
      tags: { $eq: item.tags },
      multiTag: true,
      date: { $exists: false }
    },
    modifier
  });
  // all tags with date
  updates.push({
    query: {
      group: { $exists: false },
      source: { $exists: false },
      tags: { $eq: item.tags },
      multiTag: true,
      date: { $eq: date }
    },
    modifier
  });

  // group with all tags and source
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $eq: item.source },
      tags: { $eq: item.tags },
      multiTag: true,
      date: { $exists: false }
    },
    modifier
  });
  // group with all tags and date
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $exists: false },
      tags: { $eq: item.tags },
      multiTag: true,
      date: { $eq: date }
    },
    modifier
  });
  // group with date and source
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $eq: item.source },
      tags: { $exists: false },
      multiTag: true,
      date: { $eq: date }
    },
    modifier
  });
  // date with all tags and source
  updates.push({
    query: {
      group: { $exists: false },
      source: { $eq: item.source },
      tags: { $eq: item.tag },
      multiTag: true,
      date: { $eq: date }
    },
    modifier
  });

  // group with all tags, source and date
  updates.push({
    query: {
      group: { $eq: item.group },
      source: { $eq: item.source },
      tags: { $eq: item.tag },
      multiTag: true,
      date: { $eq: date }
    },
    modifier
  });
  return updates;
}

/*
  Pivot.search({
    group: 456789
  });

  Pivot.search({
    group: 456789,
    date: '2018'
  });

  Pivot.search({
    group: 456789,
    source: 'abc.com.py'
  });

  Pivot.search({
    tagsAll: ['A', 'B'],
    tagsSize: 2
  });

  Pivot.search({
    tagsExact: ['A', 'B']
  });

  Pivot.search({
    tagsAny: ['A', 'B', 'C'],
    tagsSize: 2
  });

  Pivot.search({
    tagsExact: ['A'],
    multiTag: false
  });

  Pivot.search({
    date: '2018'
  });

  Pivot.search({
    date: '2018-10'
  });

  Pivot.search({
    date: '2018-10-01'
  });

  Pivot.search({
    date: '2018',
    sumAll: true
  });
*/
PivotSchema.statics.search = function(params) {
  let query = {};
  if(!_.isUndefined(params.multiTag)) {
    query.multiTag = params.multiTag;
  }

  // group
  if(params.group) {
    query.group = { $eq: params.group };
  } else {
    query.group = { $exists: false };
  }

  // source
  if(params.source) {
    query.source = { $eq: params.source };
  } else {
    query.source = { $exists: false };
  }

  // all sources
  if(params.sourceAll) {
    query.source = { $exists: true };
  }

  // tags
  if(params.tagsAll || params.tagsAny || params.tagsExact || params.tagsSize) {
    query.tags = query.tags || {};
    if(params.tagsAll && _.isString(params.tagsAll)) {
      params.tagsAll = JSON.parse(params.tagsAll);
    }
    if(params.tagsAny && _.isString(params.tagsAny)) {
      params.tagsAny = JSON.parse(params.tagsAny);
    }
    if(params.tagsExact && _.isString(params.tagsExact)) {
      params.tagsExact = JSON.parse(params.tagsExact);
    }
    if(params.tagsAll) {
      query.tags.$all = params.tagsAll;
    }
    if(params.tagsAny) {
      query.tags.$in = params.tagsAny;
    }
    if(params.tagsExact) {
      query.tags.$all = params.tagsExact;
      query.tags.$size = params.tagsExact.length;
    }
    if(params.tagsSize) {
      query.tags.$size = params.tagsSize;
    }
  }
  else {
    query.tags = { $exists: false };
  }

  // date
  if(params.date) {
    // YYYY
    if(params.date.length === 4) {
      query.date = { $regex: `^${params.date}.*`, $options: 'i' };
    }
    // YYYY-MM
    else if(params.date.length === 7) {
      query.date = { $regex: `^${params.date}.*`, $options: 'i' };
    }
    // YYYY-MM-DD
    else if(params.date.length === 10) {
      query.date = { $regex: params.date, $options: '$i' };
    }
    // ! Error
    else {
      throw new Error('Formato de fecha invalido! utilice uno de los siguientes formatos: "YYYY", "YYYY-MM", "YYYY-MM-DD"');
    }
  } else {
    query.date = { $exists: false };
  }

  let pipeline = [{
    $match: query
  }];
  if(params.sumAll) {
    pipeline.push({
      $group: {
        _id: 'sumAll',
        count: {$sum: '$count'}
      }
    });
  }
  return this.aggregate(pipeline);
};

export default mongoose.model('Pivot', PivotSchema);
