/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
  // Insert routes below
  // app.use('/api/things', require('./api/thing'));
  app.use('/api/items', require('./api/item'));
  app.use('/api/viz', require('./api/viz'));
  // Feeds rss2, json1, atom1
  app.use('/feeds', require('./api/feeds'));
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|feeds/auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the app.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    });
}
