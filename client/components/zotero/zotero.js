import angular from 'angular';
import zoteroService from './zotero.service';

export default angular.module('antiPyrawebsApp.zotero', [])
  .factory('zotero', zoteroService)
  .run(['zotero', '$http', function(zotero, $http) {
    /////////////////////////////////////////////////
    /// Item search api test
    /////////////////////////////////////////////////
    // $http.post('/api/items/search', {
    //   query: {
    //     group: 475109,
    //     tagsAll: ["educacion", "ciberseguridad"],
    //     source: "mitic.gov.py",
    //     year: '2019'
    //   },
    //   page: 1,
    //   limit: 10,
    //   sort: {accessDate: -1}
    // }).then(result => {
    //   console.log('item search', result.data);
    //   console.log('date', _.map(result.data.docs, 'accessDate'));
    // });

    /////////////////////////////////////////////////
    /// Pivot search test
    /////////////////////////////////////////////////
    // $http.post('/api/viz/search', [{
    //   group: 475109,
    //   sourceAll: true
    // }]).then(result => {
    //   console.log(result.data);
    // });
    //
    // $http.post('/api/viz/search', {
    //   group: 475109
    // }).then(result => {
    //   console.log('caso 1', result.data);
    // });
    //
    // $http.post('/api/viz/search', [{
    //   group: 475109
    // }, {
    //   group: 2235866
    // }]).then(result => {
    //   console.log('caso 2', result.data);
    // });
    //
    // $http.post('/api/viz/search', [{
    //   key: {itemId: _.uniqueId(), foo: 'bar'},
    //   group: 475109
    // }, {
    //   key: _.uniqueId(),
    //   group: 2235866
    // }]).then(result => {
    //   console.log('caso 3', result.data);
    // });
  }])
  .name;
