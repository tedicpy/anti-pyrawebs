'use strict';

import Pivot from './pivot.model';
import Item from '../item/item.model';
import { documentStream } from '../../components/utils';
import Promise from 'bluebird';
import _ from 'lodash';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function respond(res, statusCode) {
  statusCode = statusCode || 200;
  return function() {
    return res.sendStatus(statusCode);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.error(err);
    res.status(statusCode).send(err);
  };
}

export function search(req, res) {
  let params = req.body;
  if(!_.size(params)) {
    return res.sendStatus(413);
  }
  if(_.isArray(params)) {
    return Promise.map(params, query => {
      return Pivot.search(query)
        .then(result => {
          return {
            key: query.key,
            result
          };
        });
    })
    .then(respondWithResult(res))
    .catch(handleError(res));
  }
  else {
    return Pivot.search(params)
      .then(respondWithResult(res))
      .catch(handleError(res));
  }
}

export function rebuild(req, res) {
  let i = 0;
  return Pivot.find({}).remove()
    .then(() => {
      return documentStream(Item.find({_type: {$ne: 'attachment'}}), item => {
        i++;
        return Pivot.add(item);
      })
    })
    .then(respond(res))
    .then(() => {
      console.log(i);
    })
    .catch(handleError(res));
}

export function rebuildByTag(req, res) {
  let i = 0;
  return Pivot.find({tags: {$in: [req.params.tag]}}).remove()
    .then(() => {
      return documentStream(Item.find({tags: {$in: [req.params.tag]}}), item => {
        i++;
        console.log(_.pick(item, ['_id', 'tags']));
        return Pivot.add(item);
      })
    })
    .then(respond(res))
    .then(() => {
      console.log(i);
    })
    .catch(handleError(res));
}
