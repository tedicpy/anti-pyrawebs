'use strict';

export function routeConfig($urlRouterProvider, $locationProvider) {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode(true);
}

export function headingRun($rootScope, $state, zotero) {
  'ngInject';

  $rootScope.$state = $state;
  $rootScope.zotero = zotero;
}
