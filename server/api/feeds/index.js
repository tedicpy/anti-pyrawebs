'use strict';

var express = require('express');
var controller = require('./feeds.controller');

var router = express.Router();

// http://domain/feeds / rss2/items / ?group=475109&tags=datos-personales__privacidad&year=2019
router.get('/:feedType/:resource', controller.itemsFeed);

module.exports = router;