'use strict';

var express = require('express');
var controller = require('./viz.controller');
var config = require('../../config/environment');

var router = express.Router();

router.post('/search', controller.search);

if(config.env === 'development') {
  router.get('/rebuild/:tag', controller.rebuildByTag);
  router.get('/rebuild', controller.rebuild);
}

module.exports = router;
