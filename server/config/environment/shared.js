'use strict';

exports = module.exports = {
  zoteroDomain: 'https://api.zotero.org',
  pageSize: 10,
  lastYears: 4,
  dateTimeFormat: 'dd-MM-yyyy HH:mm',
  abstractNoteLength: 200,
  syncInterval: {
    hours: 1
  },
  feedsLimit: 20,

  main: {
    domain: 'https://anti.pyrawebs.org',
    title: 'Observatorio Antipyrawebs',
    description: 'Es un repositorio en línea y libre que mapea de manera colaborativa las iniciativas del sector privado, estatal, academia y sociedad civil de diferentes países de latinoamérica',
    author: {
      name: 'TEDIC Tecnología y Comunidad',
      // email: '',
      link: 'https://www.tedic.org'
    }
  },

  groups: [{
    id: 475109,
    name: 'Paraguay'
  }],

  tags: [{
    id: 'alianzas-publico-privadas',
    name: 'Alianzas público privadas'
  }, {
    id: 'ciberseguridad',
    name: 'Ciberseguridad'
  }, {
    id: 'conectividad',
    name: 'Conectividad'
  }, {
    id: 'covid19',
    name: 'COVID19'
  }, {
    id: 'datos-personales',
    name: 'Datos personales'
  }, {
    id: 'democracia',
    name: 'Democracia'
  }, {
    id: 'derecho-de-autor',
    name: 'Derecho de autor'
  }, {
    id: 'educacion',
    name: 'Educación'
  }, {
    id: 'genero',
    name: 'Género'
  }, {
    id: 'innovacion',
    name: 'Innovación'
  }, {
    id: 'libertad-de-expresion',
    name: 'Libertad de expresión'
  }, {
    id: 'otros',
    name: 'Otros'
  }, {
    id: 'privacidad',
    name: 'Privacidad'
  }, {
    id: 'regulacion',
    name: 'Regulación'
  }, {
    id: 'voto-electronico',
    name: 'Voto electrónico'
  }]
};
