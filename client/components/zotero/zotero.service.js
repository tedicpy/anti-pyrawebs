import _ from 'lodash';
import fileType from '../fileType-11-1-0';

export default function zoteroService($http, appConfig, $rootScope, $timeout, $state, $window, $filter, $location) {
  'ngInject';
  let $limitTo = $filter('limitTo');

  return {
    config: appConfig,
    currentLimitPage: 0,
    currentPage: 0,
    totalResults: 0,
    init: false,
    pages: [],
    selectedGroup: null,
    selectedTags: {},
    selectedYear: null,
    selectedItem: null,
    selectedItems: null,
    tags: appConfig.tags,
    groups: appConfig.groups,
    loading: false,

    getPage(page) {
      this.loading = true;
      this.totalResults = 0;
      let query = {
        page,
        sort: '-accessDate',
        limit: appConfig.pageSize
      };
      if(this.selectedGroup) {
        query.group = this.selectedGroup.id;
      }
      if(_.size(this.selectedTags)) {
        query.tagsAll = _.map(this.selectedTags, 'id');
      }
      if(this.selectedYear) {
        query.year = this.selectedYear.id;
      }
      return $http.post('/api/items/search', query)
      .then(result => {
        this.totalResults = result.data.totalDocs;
        _.forEach(result.data.docs, item => {
          // limitar la cantidad de texto a mostrar
          if(item.data.abstractNote && item.data.abstractNote.length >= appConfig.abstractNoteLength) {
            item.data.abstractNote = $limitTo(item.data.abstractNote, appConfig.abstractNoteLength);
            item.data.abstractNote += '...';
            item.data.abstractNoteOverflow = true;
          }
        });
        this.selectedItems = result.data.docs || [];
        this.hasResults = !!this.selectedItems.length;
        this.loading = false;
        this.init = true;
        this.currentPage = page;
        // $window.scrollTo(0, 0);
        return this.selectedItems;
      });
    },

    getGroup(id) {
      if(_.isString(id)) {
        id = parseInt(id);
      }
      return _.find(appConfig.groups, {id});
    },

    getGroupByName(name) {
      return _.find(appConfig.groups, {name});
    },

    getTag(tagId) {
      let tag = _.find(appConfig.tags, tag => {
        return tag.id.toLowerCase() === tagId.toLowerCase();
      });
      return tag;
    },

    clear() {
      this.pages = [];
      this.currentPage = 0;
      this.currentLimitPage = 0;
      this.selectedItems = [];
      this.init = false;
      this.totalResults = 0;
      return this;
    },

    clearFilters() {
      this.selectedGroup = null;
      this.selectedTags = {};
      this.selectedYear = null;
    },

    setSelectedGroupByName(groupName) {
      this.selectedGroup = this.getGroupByName(groupName);
      return this;
    },

    setSelectedTagById(tagId) {
      if(tagId) {
        this.selectedTags[tagId] = _.find(appConfig.tags, {id: tagId});
      }
      return this;
    },

    setSelectedYear(year) {
      if(year) {
        this.selectedYear = _.find(this.getYears(), {name: year});
      }
      return this;
    },

    getYears() {
      if(this.years) {
        return this.years;
      }
      let current = (new Date()).getFullYear();
      let years = _.range(current, current - appConfig.lastYears);
      this.years = _.map(years, year => {
        return {
          id: year,
          name: year.toString()
        };
      });
      return this.years;
    },

    // si item tiene attachments
      // descargar file de zotero con el primer key de item.attachments[]
      // segun el contentType del attachment
        // si es "image/jpg" o "image/png" o "application/pdf"
          // asignar contentType
        // si es "text/html"
          // asignar "application/octet-stream" como contentType
    downloadAttachment(item) {
      let attachmentToDownload = item.attachments[0];
      if(item.attachments.length) {
        // show alert
        item.downloading = true;
        let path = `/api/items/${attachmentToDownload}/get-file`;
        return $http.get(path, {responseType: 'arraybuffer'})
          .then(result => {
            let buffer = result.data;
            let objectUrl = window.URL || window.webkitURL;
            let blob = new Blob([buffer]);
            let fType = fileType(buffer) || {ext: 'html'};
            let fileName = `${item.key}-${attachmentToDownload}.${fType.ext}`;
            let link = document.createElement('a');
            link.download = fileName;
            link.href = objectUrl.createObjectURL(blob);
            link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
            // clean up
            link.remove();
            objectUrl.revokeObjectURL(blob);
            // hide alert
            $timeout(() => {
              item.downloading = false;
            }, 3000);
          })
          .catch(err => {
            console.log('catch');
            console.error(err);
          });
      }
    },

    generateFeedLink(feedType) {
      let path = `${$location.protocol()}://${$location.host()}`;
      if($location.port()) {
        path += `:${$location.port()}`;
      }
      path += `/feeds/${feedType}/items`;
      let query = [];
      if(this.selectedGroup) {
        query.push(`group=${this.selectedGroup.id}`);
      }
      if(_.size(this.selectedTags)) {
        let tags = _.map(this.selectedTags, 'id').join('__');
        query.push(`tags=${tags}`);
      }
      if(this.selectedYear) {
        query.push(`year=${this.selectedYear.id}`);
      }
      if(query.length) {
        path += '?' + query.join('&');
      }
      return path;
    },

    generateItemLink(item) {
      let path = `${$location.protocol()}://${$location.host()}`;
      if($location.port()) {
        path += `:${$location.port()}`;
      }
      path += `/item/${item.key}`;
      return path;
    },

    showItem(itemKey) {
      this.loading = true;
      return $http.get(`/api/items/${itemKey}`)
        .then(result => {
          this.selectedItem = result.data;
          this.loading = false;
          this.init = true;
        });      
    }
  };
}
